package id.ac.ui.cs.advprog.tutorial1.strategy;

public class MallardDuck extends Duck {
    // TODO Complete me!

    public MallardDuck() {
        setFlyBehavior(new FlyRocketPowered());
        setQuackBehavior(new Quack());
    }

    @Override
    public void display(){
        System.out.println("hello im a mallard duck");
    }
}
