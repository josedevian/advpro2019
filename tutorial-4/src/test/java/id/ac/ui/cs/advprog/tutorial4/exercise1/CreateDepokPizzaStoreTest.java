package id.ac.ui.cs.advprog.tutorial4.exercise1;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CreateDepokPizzaStoreTest {
    private PizzaStore dpkPizzaStore;
    private Pizza cheesePizza;
    private Pizza veggiePizza;
    private Pizza clamPizza;

    @Before
    public void setUp(){
        dpkPizzaStore = new DepokPizzaStore();
        cheesePizza = dpkPizzaStore.orderPizza("cheese");
        veggiePizza = dpkPizzaStore.orderPizza("veggie");
        clamPizza = dpkPizzaStore.orderPizza("clam");
    }

    @Test
    public void testCheesePizza(){
        assertEquals("---- Depok Style Cheese Pizza ----\n" +
                "ThickCrust style extra thick crust dough\n" +
                "Tomato sauce with plum tomatoes\n" +
                "Shredded Parmesan\n", cheesePizza.toString());

    }

    @Test
    public void testVeggiePizza(){
        assertEquals("---- Depok Style Veggie Pizza ----\n" +
                "ThickCrust style extra thick crust dough\n" +
                "Tomato sauce with plum tomatoes\n" +
                "Shredded Parmesan\n" +
                "Eggplant, Spinach, Black Olives, Mushrooms\n", veggiePizza.toString());
    }

    @Test
    public void testClamPizza(){
        assertEquals("---- Depok Style Clam Pizza ----\n" +
                "ThickCrust style extra thick crust dough\n" +
                "Tomato sauce with plum tomatoes\n" +
                "Shredded Parmesan\n" +
                "Frozen Clams from Chesapeake Bay\n", clamPizza.toString());
    }
}
