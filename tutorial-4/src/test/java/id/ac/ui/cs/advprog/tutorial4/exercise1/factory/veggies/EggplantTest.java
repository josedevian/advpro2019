package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Eggplant;
import org.junit.Before;
import org.junit.Test;

public class EggplantTest {
    private Eggplant eggplant;

    @Before
    public void setUp(){
        eggplant = new Eggplant();
    }

    @Test
    public void testtoStringMethod(){
        assertEquals("Eggplant", eggplant.toString());
    }

}