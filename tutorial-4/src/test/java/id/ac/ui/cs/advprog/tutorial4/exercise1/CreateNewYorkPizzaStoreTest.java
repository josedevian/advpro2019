package id.ac.ui.cs.advprog.tutorial4.exercise1;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CreateNewYorkPizzaStoreTest {
    private PizzaStore nyPizzaStore;
    private Pizza cheesePizza;
    private Pizza veggiePizza;
    private Pizza clamPizza;

    @Before
    public void setUp(){
        nyPizzaStore = new NewYorkPizzaStore();
        cheesePizza = nyPizzaStore.orderPizza("cheese");
        veggiePizza = nyPizzaStore.orderPizza("veggie");
        clamPizza = nyPizzaStore.orderPizza("clam");
    }

    @Test
    public void testCheesePizza(){
        assertEquals("---- New York Style Cheese Pizza ----\n" +
                "Thin Crust Dough\n" +
                "Marinara Sauce\n" +
                "Reggiano Cheese\n", cheesePizza.toString());
    }

    @Test
    public void testVeggiePizza(){
        assertEquals("---- New York Style Veggie Pizza ----\n" +
                "Thin Crust Dough\n" +
                "Marinara Sauce\n" +
                "Reggiano Cheese\n" +
                "Garlic, Onion, Mushrooms, Red Pepper\n", veggiePizza.toString());
    }

    @Test
    public void testClamPizza(){
        assertEquals("---- New York Style Clam Pizza ----\n" +
                "Thin Crust Dough\n" +
                "Marinara Sauce\n" +
                "Reggiano Cheese\n" +
                "Fresh Clams from Long Island Sound\n", clamPizza.toString());
    }
}
