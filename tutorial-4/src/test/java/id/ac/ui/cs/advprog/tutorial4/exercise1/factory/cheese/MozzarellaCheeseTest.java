package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class MozzarellaCheeseTest {
    private MozzarellaCheese mozCheese;

    @Before
    public void setUp(){
        mozCheese = new MozzarellaCheese();
    }

    @Test
    public void testtoStringMethod(){
        assertEquals("Shredded Mozzarella", mozCheese.toString());
    }

}
