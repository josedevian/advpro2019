package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.Cheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.ParmesanCheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.Clams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.FrozenClams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.Dough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.ThickCrustDough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.PlumTomatoSauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.Sauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Eggplant;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Mushroom;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Spinach;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.BlackOlives;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Veggies;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;

public class DepokPizzaIngredientFactoryTest {
    private DepokPizzaIngredientFactory factory;

    @Before
    public void setUp() {
        factory = new DepokPizzaIngredientFactory();
    }

    @Test
    public void testCreateDoughMethod() {
        Dough dough = factory.createDough();
        assertThat(dough, instanceOf(ThickCrustDough.class));
    }

    @Test
    public void testCreateSauceMethod() {
        Sauce sauce = factory.createSauce();
        assertThat(sauce, instanceOf(PlumTomatoSauce.class));
    }

    @Test
    public void testCreateCheeseMethod() {
        Cheese cheese = factory.createCheese();
        assertThat(cheese, instanceOf(ParmesanCheese.class));
    }

    @Test
    public void testCreateClamsMethod() {
        Clams clams = factory.createClam();
        assertThat(clams, instanceOf(FrozenClams.class));
    }

    @Test
    public void testCreateVeggiesMethod() {
        Veggies[] veggies = factory.createVeggies();
        for (int i = 0; i < veggies.length; i++) {
            assertThat(veggies[i], instanceOf(Veggies.class));
        }
    }
}
