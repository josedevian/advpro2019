package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class CheddarCheeseTest {
    private CheddarCheese cheddarCheese;

    @Before
    public void setUp(){
        cheddarCheese = new CheddarCheese();
    }

    @Test
    public void testtoStringMethod(){
        assertEquals("Shredded Cheddar", cheddarCheese.toString());
    }

}
