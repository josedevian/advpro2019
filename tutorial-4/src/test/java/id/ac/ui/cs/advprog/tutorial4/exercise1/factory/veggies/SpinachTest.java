package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Spinach;
import org.junit.Before;
import org.junit.Test;

public class SpinachTest {
    private Spinach spinach;

    @Before
    public void setUp(){
        spinach = new Spinach();
    }

    @Test
    public void testtoStringMethod(){
        assertEquals("Spinach", spinach.toString());
    }

}