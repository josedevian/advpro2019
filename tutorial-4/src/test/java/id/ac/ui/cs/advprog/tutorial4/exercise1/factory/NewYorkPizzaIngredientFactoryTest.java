package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.Cheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.ReggianoCheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.Clams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.FreshClams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.Dough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.ThinCrustDough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.MarinaraSauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.Sauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Veggies;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;

public class NewYorkPizzaIngredientFactoryTest {
    private NewYorkPizzaIngredientFactory factory;

    @Before
    public void setUp() {
        factory = new NewYorkPizzaIngredientFactory();
    }

    @Test
    public void testCreateDoughMethod() {
        Dough dough = factory.createDough();
        assertThat(dough, instanceOf(ThinCrustDough.class));
    }

    @Test
    public void testCreateSauceMethod() {
        Sauce sauce = factory.createSauce();
        assertThat(sauce, instanceOf(MarinaraSauce.class));
    }

    @Test
    public void testCreateCheeseMethod() {
        Cheese cheese = factory.createCheese();
        assertThat(cheese, instanceOf(ReggianoCheese.class));
    }

    @Test
    public void testCreateClamsMethod() {
        Clams clams = factory.createClam();
        assertThat(clams, instanceOf(FreshClams.class));
    }

    @Test
    public void testCreateVeggiesMethod() {
        Veggies[] veggies = factory.createVeggies();
        for (int i = 0; i < veggies.length; i++) {
            assertThat(veggies[i], instanceOf(Veggies.class));
        }
    }
}
