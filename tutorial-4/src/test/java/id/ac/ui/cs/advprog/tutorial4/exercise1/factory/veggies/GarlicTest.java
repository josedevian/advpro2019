package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Garlic;
import org.junit.Before;
import org.junit.Test;

public class GarlicTest {
    private Garlic garlic;

    @Before
    public void setUp(){
        garlic = new Garlic();
    }

    @Test
    public void testtoStringMethod(){
        assertEquals("Garlic", garlic.toString());
    }

}