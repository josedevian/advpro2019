package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.BlackOlives;
import org.junit.Before;
import org.junit.Test;

public class BlackOlivesTest {
    private BlackOlives blackOlives;

    @Before
    public void setUp(){
        blackOlives = new BlackOlives();
    }

    @Test
    public void testtoStringMethod(){
        assertEquals("Black Olives", blackOlives.toString());
    }

}