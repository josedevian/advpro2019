package id.ac.ui.cs.advprog.tutorial3.composite.higherups;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class Ceo extends Employees {
    public Ceo(String name, double salary) {
        if(salary<200000000){
            throw new IllegalArgumentException();
        }
        this.name = name;
        this.salary = salary;
        role="CEO";
    }

    @Override
    public double getSalary() {
        return salary;
    }
}
