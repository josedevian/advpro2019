package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;
import jdk.nashorn.internal.runtime.regexp.joni.exception.ValueException;

public class SecurityExpert extends Employees {
    public SecurityExpert(String name, double salary) {
        if(salary<70000000){
            throw new IllegalArgumentException();
        }
        this.name = name;
        this.salary = salary;
        role = "Security Expert";
    }

    @Override
    public double getSalary() {
        return salary;
    }

}
